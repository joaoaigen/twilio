const {Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn} = require("typeorm");

@Entity()
export default class Response {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    response: string;

    @Column()
    url_callback: string;

    @Column()
    params: string;

    @CreateDateColumn()
    created_at: string;

    @UpdateDateColumn({ type: "timestamp"})
    updated_at: string;
}
