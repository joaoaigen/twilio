const {Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn} = require("typeorm");

@Entity()
export default class Users {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    key: string;

    @Column()
    name_of_solution: string;

    @CreateDateColumn()
    created_at: string;

    @UpdateDateColumn({ type: "timestamp"})
    updated_at: string;
}
