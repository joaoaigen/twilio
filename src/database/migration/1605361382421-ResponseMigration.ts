import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class ResponseMigration1605361382421 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        queryRunner.createTable(new Table({
            name: "response",
            columns: [
                {
                    name: "id",
                    type: "varchar",
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "uuid"
                },
                {
                    name: "response",
                    type: "varchar",
                },
                {
                    name: "url_callback",
                    type: "varchar"
                },
                {
                    name: "params",
                    type: "varchar"
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                },
            ]

        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.dropTable('response');
    }

}
