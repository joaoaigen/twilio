import express from 'express';
import routes from './routes';
import './configs/connectionDatabase';

const http = require('http');
const bodyParser = require('body-parser');


const app = express();

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(routes);

http.createServer(app).listen(3000, () => {
    console.log('Express server listening on port 3000');
});
