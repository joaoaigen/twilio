import {Request, Response, Router} from 'express';

import TwilioController from "./controllers/TwilioController";
import ResponseController from "./controllers/ResponseController";

const routes = Router();


//CHATBOT FUNCTIONS
routes.get('/sendMessage/', TwilioController.sendMessage);
routes.post('/receiveMessage', TwilioController.receiveMessage);

routes.post('/teste', ResponseController.store)

export default routes;
