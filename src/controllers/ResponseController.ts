import {Request, Response} from "express";
import ResponseEntity from "../database/entity/response";
import {getRepository} from "typeorm";

export default {

    async store(request: Request, resp: Response) {
        const { url_callback, response, params } = request.body;
        const data = {
             url_callback, response, params
        }
        const responseRepository = getRepository(ResponseEntity);
        const result = responseRepository.create(data)
        try{
            await responseRepository.save(result);
            return resp.status(201).json(result)
        }catch(err){
            return resp.status(400).json(err)
        }

    },

}
