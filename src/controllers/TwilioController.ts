import {Request, Response} from "express";

import * as Yup from 'yup';
import ResponseEntity from '../database/entity/response';
import {getRepository} from "typeorm";

const configs = require('../configs/configs');
const MessagingResponse = require('twilio').twiml.MessagingResponse;


export default {

    async sendMessage(request: Request, response: Response) {
        //request vars
        const {phone_number} = request.query;

        const data = {
            phone_number,
        };

        //request validate
        const schema = Yup.object().shape({
            phone_number: Yup.number().required(),
        });

        try {
            await schema.validate(data, {
                abortEarly: false,
            });
        } catch (err) {
            return response.json({
                error: err.errors,
            });
        }

        //twilio vars and imports
        const accountSid = configs.default.twilio_account_sid;
        const authToken = configs.default.twilio_auth_token;
        const client = require('twilio')(accountSid, authToken);

        try{
            await client.messages
                .create({
                    from: 'whatsapp:+14155238886',
                    body: 'teste',
                    to: 'whatsapp:' + phone_number
                })
                .then((message: any) => {
                    return response.status(200).json(message);
                })
        }catch(err){
            return response.status(400).json(err.message);
        }
    },

    async receiveMessage(request: Request, response: Response) {
        const twiml = new MessagingResponse();
        const From = request.body.From;
        const Body = request.body.Body;
        const formatFrom = From.split(':')[1];
        const responseRepository = getRepository(ResponseEntity);

        try {
            const result = await responseRepository.find({where: {response: Body}});

            //Verifica se esta vazio
            var isEmpty = function(result: object) {
                return Object.keys(result).length === 0;
            }
            if (isEmpty(result)) {
                const all = await responseRepository.find();
                twiml.message('Por favor diga algum desses comandos! ( *```' + all.map(res => {
                    return res.response;
                })+'```*)')
                response.writeHead(200, {'Content-Type': 'text/xml'});
                response.end(twiml.toString());
            }

            console.log(result)
        } catch (e) {
            twiml.message('Comando invalido!')
            response.writeHead(200, {'Content-Type': 'text/xml'});
            response.end(twiml.toString());
        }
    },

}
